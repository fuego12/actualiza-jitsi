#/bin/bash

cp /root/CarpetaTemporal/css/all.css /usr/share/jitsi-meet/css/all.css
cp /root/CarpetaTemporal/interface_config.js /usr/share/jitsi-meet/interface_config.js
cp -r -u /usr/share/jitsi-meet/images /root/CarpetaTemporal/images
rm -r /usr/share/jitsi-meet/images
cp -r /root/CarpetaTemporal/images /usr/share/jitsi-meet/images