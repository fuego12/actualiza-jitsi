#/usr/bin/python3

import os

# Chequeo de si se cambio el archivo /usr/share/jitsi-meet/css/all.css
with open ("/root/Hashes/Hash_azul_viejo_all.py", "r") as f:
	hash_azul_viejo_all = f.read()

os.system("sha256sum /usr/share/jitsi-meet/css/all.css > /root/CarpetaTemporal/Hash_azul_nuevo_all.py")
with open ("/root/CarpetaTemporal/Hash_azul_nuevo_all.py", "r") as f:
	hash_azul_nuevo_all = f.read()

if str(hash_azul_viejo_all) != hash_azul_nuevo_all:
	for i in range (10):
		print("Hubo un cambio en el archivo /usr/share/jitsi-meet/css/all.css")

os.system("echo " + hash_azul_nuevo_all + " > /root/Hashes/Hash_azul_viejo_all.py")



# Chequeo de si se cambio el archivo /usr/share/jitsi-meet/interface_config.js
with open ("/root/Hashes/Hash_azul_viejo_int.py", "r") as f:
	hash_azul_viejo_int = f.read()

os.system("sha256sum /usr/share/jitsi-meet/interface_config.js > /root/CarpetaTemporal/Hash_azul_nuevo_int.py") 
with open ("/root/CarpetaTemporal/Hash_azul_nuevo_int.py", "r") as f:
	hash_azul_nuevo_int = f.read()

if str(hash_azul_viejo_int) != hash_azul_nuevo_int:
	for i in range (10):
		print("Hubo un cambio en el archivo /usr/share/jitsi-meet/interface_config.js")

os.system("echo " + hash_azul_nuevo_int + " > /root/Hash_azul_viejo_int.py")
