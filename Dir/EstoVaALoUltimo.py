#/usr/bin/python3

ip_privada = "192.168.0.63"
ip_publica = "181.170.31.9"

with open ("/etc/jitsi/videobridge/sip-communicator.properties", "r") as a:
    b = a.read()
    b = b.replace("true\norg.ice4j.ice.harvest.STUN_MAPPING_HARVESTER_ADDRESSES=meet-jit-si-turnrelay.jitsi.net:443", "true\n# org.ice4j.ice.harvest.STUN_MAPPING_HARVESTER_ADDRESSES=meet-jit-si-turnrelay.jitsi.net:443")
    if not "org.ice4j.ice.harvest.NAT_HARVESTER_LOCAL_ADDRESS" in b:
        b = b + "org.ice4j.ice.harvest.NAT_HARVESTER_LOCAL_ADDRESS=" + str(ip_privada) + "\norg.ice4j.ice.harvest.NAT_HARVESTER_PUBLIC_ADDRESS=" + str(ip_publica)

with open ("/etc/jitsi/videobridge/sip-communicator.properties", "w") as a:
    a.write(b)