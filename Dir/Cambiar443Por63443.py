#/usr/bin/python3

import os

rutas_443 = ['/etc/jitsi/meet/vostok.duckdns.org-config.js', '/etc/prosody/conf.avail/vostok.duckdns.org.cfg.lua', '/etc/prosody/conf.d/vostok.duckdns.org.cfg.lua', '/etc/nginx/modules-enabled/60-jitsi-meet.conf', '/etc/nginx/sites-enabled/vostok.duckdns.org.conf', '/etc/nginx/sites-available/vostok.duckdns.org.conf']

for i in rutas_443:
    a = open(i, "r")
    b = a.read()
    b.replace(" 443", " 63443")
    b.replace(":443", ":63443")
    b.replace('"443', '"63443')
    a.close()
    c = open(i, "w")
    c.write(b)
    c.close()
  

  
rutas_80 = ['/etc/nginx/sites-enabled/vostok.duckdns.org.conf', '/etc/nginx/sites-available/vostok.duckdns.org.conf']

for i in rutas_80:
    a = open(i, "r")
    b = a.read()
    b.replace(" 80", " 63080")
    b.replace(":80", ":63080")
    c = open(i, "w")
    c.write(b)
    c.close()
